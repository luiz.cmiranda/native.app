import React, { useState } from "react";
import { View, TextInput, StyleSheet, Button, Modal } from "react-native";

export const GoalInput = (props) => {
  const [enteredGoal, setEnteredGoal] = useState("");

  const goalInputHandler = (enteredText) => {
    setEnteredGoal(enteredText);
  };

  const addGoalHandler = () => {
    props.onAddGoal(enteredGoal)
    setEnteredGoal('')
  }

  return (
    <Modal visible={props.visible} animationType="slide">
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Metas do Curso"
          style={styles.input}
          onChangeText={goalInputHandler}
          value={enteredGoal}
        />
        <View style={styles.buttonContainer}>
         <View Style={styles.button}> 
        <Button
          title="Cancelar"
          color="red"
          onPress={props.onCancel}
        />    
         </View>
         <View Style={styles.button}> 
        <Button
          title="Adicionar"
          onPress={addGoalHandler}
        />
           </View>
          </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flex:1,
    justifyContent: "center",
    alignItems: "center",
  },

  input: {
    width: "80%",
    height: 30,
    borderColor: "black",
    borderWidth: 1,
    textAlign: "center",
    marginBottom: 10,
    borderRadius:5
  },

  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "60%",

  },

  button: {
    width: "40%",
  }
});
